package com.ir.objects;


/**
 * A Square on a Chess Board
 * @author Mark Feaver
 */
public class Square {
	private int xPosition, yPosition;
	private Square up, right, down, left = null;
	private Board board;
	
	/**
	 * Constructor - creates a square on a chess board
	 * @param xPosition - the column position of the current square (i.e. A, B, C, etc)
	 * @param yPosition - the row position of the current square (i.e. 1, 2, 3, etc)
	 * @param board - the board that the square is on
	 */
	public Square(int xPosition, int yPosition, Board board){
		this.xPosition = xPosition;
		this.yPosition = yPosition;
		this.board = board;
	}
	
	/**
	 * Gets the square's X Position
	 * @return an int representing the x column
	 */
	public int getXPosition(){
		return xPosition;
	}
	
	/**
	 * Gets the square's Y Position
	 * @return an int representing the y row
	 */
	public int getYPosition(){
		return yPosition;
	}
	
	/**
	 * Gets the neighbouring square directly above
	 * @return
	 */
	public Square getUp(){
		return up;
	}
	
	/**
	 * Gets the neighbouring square directly to the right
	 * @return
	 */
	public Square getRight(){
		return right;
	}
	
	/**
	 * Gets the neighbouring square directly below
	 * @return
	 */
	public Square getDown(){
		return down;
	}
	
	/**
	 * Gets the neighbouring square directly to the left
	 * @return
	 */
	public Square getLeft(){
		return left;
	}
	
	/**
	 * Set up the references to neighbouring squares
	 */
	public void setNeighbours(){
		if (yPosition < board.getYSize()-1)
			up = board.getSquare(xPosition, yPosition+1);
		if (xPosition < board.getXSize()-1)
			right = board.getSquare(xPosition+1, yPosition);
		if (yPosition > 0)
			down= board.getSquare(xPosition, yPosition-1);
		if (xPosition > 0)
			left = board.getSquare(xPosition-1, yPosition);
	}
	
	@Override
	public String toString(){
		String pos = new String();
		pos = String.valueOf((char)(xPosition + 'A')) + Integer.toString(yPosition+1);
		return pos;
	}
}
