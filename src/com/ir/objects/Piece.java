package com.ir.objects;

import java.util.List;


/**
 * An abstract representation of a Chess Piece
 * @author Mark Feaver
 */
public abstract class Piece {
	protected enum Type {
		KNIGHT, BISHOP, ROOK,
		KING, QUEEN, PAWN
	}
	
	protected Type type;
	protected Square position;
	
	/**
	 * Constructor - takes a starting position
	 * @param position - a Square position
	 */
	protected Piece(Square position){
		this.position = position;
	}
	
	/**
	 * Gets the current position of the chess piece
	 * @return
	 */
	public Square getPosition(){
		return position;
	}
	
	/**
	 * Moves the piece to the specified position
	 * @param newPosition
	 */
	public void move(Square newPosition){
		position = newPosition;
	}
	
	/**
	 * Gets a list of valid positions that a piece may move to
	 * @return a list of valid positions
	 */
	public abstract List<Square> getValidMoves();
	
	/**
	 * Gets the "h-value" heuristic estimate of the number of moves to the finish
	 * @param from - the square we are moving from
	 * @param to - the square we are moving to
	 * @return the heuristic estimate of the number of moves to the finishing square
	 */
	public abstract int getHeuristicMoves(Square from, Square to);
}
