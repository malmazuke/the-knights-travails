package com.ir;

import java.util.List;

import com.ir.objects.Board;
import com.ir.objects.Knight;
import com.ir.objects.Path;
import com.ir.objects.Piece;
import com.ir.objects.Square;
import com.ir.search.AStarSearch;

/**
 * Finds the shortest path between two specified points, using the A* Search and
 * heuristics.
 * @author Mark Feaver
 *
 */
public class KnightsTravails {

	/**
	 * @param args - two arguments containing the starting and ending positions in algebraic chess notation
	 */
	public static void main(String[] args) {
		// If no args, print a usage statement
		if (args.length == 0){
			printUsage();
			return;
		}
		
		// If the wrong number of args, print an error
		if (args.length != 2){
			printError(0);
			return;
		}
		
		// Create a board
		Board board = new Board();
		//Debug.printBoard(board);
		
		// Create the start and end squares
		Square startSquare = board.getSquare(args[0]);
		Square endSquare = board.getSquare(args[1]);
		
		// If the squares contained incorrect positions, print an error statement
		if (startSquare == null || endSquare == null){
			printError(1);
			return;
		}
		
		// Create a knight chess piece
		Piece knight = new Knight(startSquare);
		
		// Setup our A* Search, and begin searching
		AStarSearch aStar = new AStarSearch(startSquare, endSquare, knight);
		Path p = aStar.Search();
		
		// Print the path that was discovered
		if (p != null)
			printPath(p);
		else
			printError(2);
	}
	
	/**
	 * Prints out the usage statement for the program
	 */
	public static void printUsage(){	    
		System.out.println("usage: KnightsTravails startPosition endPosition");
	}
	
	/**
	 * Prints out an error
	 * @param errType - the type of the error, from 0 - 2
	 */
	public static void printError(int errType){
		switch(errType){
		case(0):
			System.err.println("Error: Not enough input arguments");
			break;
		case(1):
			System.err.println("Error: Incorrect position specified");
			break;
		case(2):
			System.err.println("Error: Could not find a shortest path");
			break;
		default:
			System.err.println("Unknown error");
			return;
			
		}
	}

	/**
	 * Prints a given path to Standard Out (not including the starting position)
	 * @param path - a Path object containing the path from start to finish
	 */
	public static void printPath(Path path){
		List<Square> p = path.getPath(); 
		for (int i = p.size() - 2; i > 0; i--){
			Square s = p.get(i);
			System.out.print(s.toString() + " ");
		}
		Square s = p.get(0);
		System.out.println(s.toString());
	}
}
