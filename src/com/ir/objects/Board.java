package com.ir.objects;


/**
 * A Chess Board
 * @author Mark Feaver
 */
public class Board {
	private Square[][] squares;
	private int xSize, ySize;
	private static final int DEFAULT_SIZE = 8;
	
	/**
	 * Default Constructor - generates a board using the default size
	 */
	public Board(){
		xSize = ySize = DEFAULT_SIZE;
		generateSquares();
	}
	/**
	 * Constructor - generates a square board
	 * @param dimensions - the size of the board
	 */
	public Board(int dimensions){
		xSize = ySize = dimensions;
		generateSquares();
	}
	
	/**
	 * Constructor - generates a rectangular board
	 * @param xDimension - the number of columns
	 * @param yDimension - the number of rows
	 */
	public Board(int xDimension, int yDimension){
		xSize = xDimension;
		ySize = yDimension;
		generateSquares();
	}
	
	/**
	 * Gets the square at the specified position
	 * @param x - the column number (a numerical value)
	 * @param y - the row number
	 * @return the square at the specified position
	 */
	public Square getSquare(int x, int y){
		if (x < 0 || x >= xSize || y < 0 || y >= ySize)
			return null;
		return squares[x][y];
	}
	
	/**
	 * Gets the square at the specified position in algebraic chess notation
	 * @param position - the position in algebraic chess notation
	 * @return the specified square
	 */
	public Square getSquare(String position){
		if (position.length() != 2){
			return null;
		}
		
		char x = position.charAt(0);
		x = Character.toUpperCase(x);
		int xPos, yPos;
		
		xPos = (x - 'A');
		yPos = Integer.parseInt(position.substring(1, 2)) - 1;
		return getSquare(xPos, yPos);
	}
	
	/**
	 * Getter for the number of columns
	 * @return the number of columns
	 */
	public int getXSize(){
		return xSize;
	}
	
	/**
	 * Getter for the number of rows
	 * @return the number of rows
	 */
	public int getYSize(){
		return ySize;
	}
	
	/**
	 * Generates all of the squares on the board
	 * @param xDimension
	 * @param yDimension
	 */
	private void generateSquares(){
		squares = new Square[xSize][ySize];
		
		// Create all of the squares 
		for (int y = ySize-1; y >= 0; y--){
			for (int x = 0; x < xSize; x++){
				squares[x][y] = new Square(x, y, this);
			}
		} 
		// Link all of the squares to their neighbours
		for (int y = ySize-1; y >= 0; y--){
			for (int x = 0; x < xSize; x++){
				squares[x][y].setNeighbours();
			}
		}
	}
	
}
