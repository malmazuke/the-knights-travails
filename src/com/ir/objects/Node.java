package com.ir.objects;

/**
 * A Node, used in the A* Search to link squares to their associated f scores
 * @author Mark Feaver
 *
 */
public class Node implements Comparable<Node>{
	private Square square = null;
	private int fScore;
	
	/**
	 * Constructor. Takes a position and an f-score
	 * @param square - the square position of the node on the chess board
	 * @param fScore - the f-score associated with this square
	 */
	public Node(Square square, int fScore){
		this.square = square;
		this.fScore = fScore;
	}
	
	/**
	 * Gets the f-score of the node
	 * @return the f-score
	 */
	public int getFScore(){
		return fScore;
	}
	
	/**
	 * Gets the Square at the current node
	 * @return the Square
	 */
	public Square getSquare(){
		return square;
	}

	@Override
	public int compareTo(Node node) {
		if (this.fScore < node.fScore)
			return -1;
		if (this.fScore > node.fScore)
			return 1;
		return 0;
	}
}
