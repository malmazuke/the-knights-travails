package com.ir.objects;

import java.util.ArrayList;
import java.util.List;


/**
 * A Knight Piece, which extends the abstract "Piece" Object
 * @author Mark Feaver
 */
public class Knight extends Piece {

	// A hard-coded array of the number of moves required to reach a certain square.
	// Can use hardcoded values in this case, as the board is quite small, and if we
	// did not use these hard coded values, we would have to do a search to discover these
	// numbers anyway. Therefore, using the hardcoded values saves time
	private int moves[][] = new int[][]{
		{0, 3, 2, 3, 2, 3, 4, 5},
		{3, 2, 1, 2, 3, 4, 3, 4},
		{2, 1, 4, 3, 2, 3, 4, 5},
		{3, 2, 3, 2, 3, 4, 3, 4},
		{2, 3, 2, 3, 4, 3, 4, 5},
		{3, 4, 3, 4, 3, 4, 5, 4},
		{4, 3, 4, 3, 4, 5, 4, 5},
		{5, 4, 5, 4, 5, 4, 5, 6}
	};
	
	/**
	 * Constructor - takes a starting position
	 * @param position
	 */
	public Knight(Square position) {
		super(position);
		this.type = Type.KNIGHT;
	}

	@Override
	public List<Square> getValidMoves() {
		List<Square> moves = new ArrayList<Square>();
		Square square = null;
		
		// Up
		square = position.getUp();
		if (square != null){
			// Up-Left
			if (square.getUp() != null && square.getLeft() != null)
				moves.add(square.getUp().getLeft());
			// Up-Right
			if (square.getUp() != null && square.getRight() != null)
				moves.add(square.getUp().getRight());
				
		}
		
		// Right
		square = position.getRight();
		if (square != null){
			// Right-Up
			if (square.getRight() != null && square.getUp() != null)
				moves.add(square.getRight().getUp());
			// Right-Down
			if (square.getRight() != null && square.getDown() != null)
				moves.add(square.getRight().getDown());				
		}
		
		// Down
		square = position.getDown();
		if (square != null){
			// Down-Right
			if (square.getDown() != null && square.getRight() != null)
				moves.add(square.getDown().getRight());
			// Down-Left
			if (square.getDown() != null && square.getLeft() != null)
				moves.add(square.getDown().getLeft());
		}
		
		// Left
		square = position.getLeft();
		if (square != null){
			// Left-Down
			if (square.getLeft() != null && square.getDown() != null)
				moves.add(square.getLeft().getDown());	
			// Left-Up
			if (square.getLeft() != null && square.getUp() != null)
				moves.add(square.getLeft().getUp());
		}
		
		return moves;
	}
	
	@Override
	public int getHeuristicMoves(Square from, Square to){
		int rowDiff;
		if (from.getXPosition() > to.getXPosition())
			rowDiff = from.getXPosition() - to.getXPosition();
		else
			rowDiff = to.getXPosition() - from.getXPosition();

		int colDiff;
		if (from.getYPosition() > to.getYPosition())
			colDiff = from.getYPosition() - to.getYPosition();
		else
			colDiff = to.getYPosition() - from.getYPosition();

		return moves[rowDiff][colDiff];
	}

}
