package com.ir;

import com.ir.objects.Board;

/**
 * A collection of debugging methods
 * @author Mark Feaver
 *
 */
public class Debug {
	/**
	 * Prints a visual representation of the board to Standard Out
	 * @param board
	 */
	public static void printBoard(Board board){
		for (int y = board.getYSize()-1; y >= 0; y--){
			for (int x = 0; x < board.getXSize(); x++){
				System.out.print(board.getSquare(x, y).toString() + " ");
			}
			System.out.println();
		}
	}
}
