The Knight's Travails
By Mark Feaver
---------------------

Searches for the shortest path between two positions on an 8x8 chess board.

To build:
    'make'

To run:
    java -cp KnightsTravails/classes com.ir.KnightsTravails <start square> <destination square>