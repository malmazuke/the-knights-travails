package com.ir.objects;

import java.util.LinkedList;
import java.util.List;

/**
 * A path, containing a set of linked squares detailing the movements of a chess piece
 * @author Mark Feaver
 *
 */
public class Path {
	private List<Square> path = null;
	
	/**
	 * Constructor. Sets up a linked list to store linked squares
	 */
	public Path(){
		path = new LinkedList<Square>();
	}
	
	/**
	 * Adds a square to the end of the path
	 * @param square - a square in the path
	 */
	public void addSquare(Square square){
		path.add(square);
	}
	
	/**
	 * Gets the path object
	 * @return the path
	 */
	public List<Square> getPath(){
		return this.path;
	}
	
	/**
	 * Clears the path
	 */
	public void clear(){
		path = new LinkedList<Square>();
	}
}
